private static class Point{
        private int x;
        private int y;

        private Point(int x, int y) {
            this.x = x;
            this.y = y;
        }

        public int getX() {
            return x;
        }

        public void setX(int x) {
            this.x = x;
        }
        
        public Point moveRightBy(int x) {
            return new Point(this.x + x, this.y);
        }
    }
    
    public static void main(String[] args) {
        
        // teht 1
        /*
        Debugging.java-tiedostossa on Point-luokan toteutus. 
        Luennolla esitettiin moveAllPointsRightBy-metodi, jolla saadaan siirrettyä kaikki listan pisteet oikealle. 
        Toteuta tämä loppuun asti niin, että myös testit menevät läpi.
        Vinkki. AssertArrayEquals() käyttää equals()-metodia. Miten varmistetaan pisteiden yhtäsuuruus?
        */
        
        // vanha lista, jossa määritetään pisteiden koordinaatteja
        List<Point> points = Arrays.asList(new Point(12, 2));
        
        // uusi lista, jossa uusia pisteitä on siirretty oikealle
        List<Point> newpoints = points.stream().map(p -> p.moveRightBy(p.getX())).collect(toList());
        
        // tulostetaan molemmat listat ja verrataan pisteiden koordinaatteja keskenään
        System.out.println("teht 1");
        points.stream().map(p -> p.getX()).forEach(System.out::println);
        newpoints.stream().map(np -> np.getX()).forEach(System.out::println);
        
        //test
        @Test
        public void testMoveRightBy() {
            System.out.println("moveRightBy");
            
            List<Point> points = Arrays.asList(new Point(5,5), new Point(10,5));
            List<Point> expectedPoints = Arrays.asList(new Point(10,5), new Point(20,5));
            List<Point> newPoints = points.stream()
                    .map(p -> p.moveRightBy(p.getX()))
                    .collect(toList());
            
            int [] epoints = expectedPoints.stream().mapToInt(p -> p.getX()).toArray();
            int [] npoints = newPoints.stream().mapToInt(p -> p.getX()).toArray();
            
            Assert.assertArrayEquals(epoints, npoints);
        }
    }