/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package teht8;

import static java.lang.Thread.sleep;
import java.util.Observable;

/**
 *
 * @author Helen
 */
public class Uutinen extends Observable implements Runnable{
    
    @Override
    public void run() {
        String[] uutiset = {"Hallitus kaatui! #politiikka", "Pekka Töpöhännästä uusi kansalliseläin? #julkkis", "Cheek menee eläkkeelle #julkkis", "Budjettia kasvatetaan #politiikka", "Gaalassa tapahtui nolo juttu! #julkkis", "Lakiesitys kaatui huutokisaan! #politiikka"};
        for (String uutiset1 : uutiset) {
            try {
                setChanged();
                notifyObservers(uutiset1);
                sleep(100);
            }catch (InterruptedException ex) {
            }
        }
    }
    
}
