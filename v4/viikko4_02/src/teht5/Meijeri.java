/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package teht5;

import java.util.HashMap;
import java.util.Map;
import java.util.function.Supplier;

/**
 *
 * @author Helen
 */
public class Meijeri {
    Map<String, Supplier<Maito>> maidot;
    Map<String, Supplier<Jugurtti>> jugurtit;
    Map<String, Supplier<Juusto>> juustot;
    
    public Meijeri() {
        maidot = new HashMap<>();
        jugurtit = new HashMap<>();
        juustot = new HashMap<>();
    }
    
    public Maito getMaito(String tuoteType) {
        Supplier<Maito> maito = maidot.get(tuoteType);
        if(maito != null) {
            return maito.get();
        }
        throw new IllegalArgumentException("Ei löytynyt " + tuoteType);
    }
    
    public Jugurtti getJugurtti(String tuoteType) {
        Supplier<Jugurtti> jugurtti = jugurtit.get(tuoteType);
        if(jugurtti != null) {
            return jugurtti.get();
        }
        throw new IllegalArgumentException("Ei löytynyt " + tuoteType);
    }
    
    public Juusto getJuusto(String tuoteType) {
        Supplier<Juusto> juusto = juustot.get(tuoteType);
        if(juusto != null) {
            return juusto.get();
        }
        throw new IllegalArgumentException("Ei löytynyt " + tuoteType);
    }
}
