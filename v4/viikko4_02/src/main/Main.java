/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package main;

import java.text.Normalizer;
import java.util.ArrayList;
import java.util.List;
import java.util.Observable;
import java.util.function.Function;
import java.util.function.UnaryOperator;
import java.util.regex.Pattern;
import java.util.stream.Stream;
import teht4.Omena;
import teht5.Jugurtti;
import teht5.Juusto;
import teht5.Laktoositon;
import teht5.Maito;
import teht5.Meijeri;
import teht5.Vahalaktoosinen;
import teht6.Dokumentti;
import teht7.StrategyData;
import teht8.Uutinen;

/**
 *
 * @author Helen
 */
public class Main {

    /**
     * @param args the command line arguments
     */
    public static final Pattern DIACRITICS_AND_FRIENDS
    = Pattern.compile("[\\p{InCombiningDiacriticalMarks}\\p{IsLm}\\p{IsSk}]+");
    
    public static void main(String[] args){
        
        //teht 4
        System.out.println("\nteht 4");
        Omena o1 = new Omena("vihreä", 100);
        Omena o2 = new Omena("vihreä", 110);
        Omena o3 = new Omena("punainen", 101);
        Omena o4 = new Omena("punainen", 105);
        
        List<Omena> omenat = new ArrayList<>();
        
        omenat.add(o1);
        omenat.add(o2);
        omenat.add(o3);
        omenat.add(o4);
            
        Stream<Omena> omenaStream = omenat.stream();
        List<Omena> omenaLista = omenaStream.collect(ArrayList::new, List::add, List::addAll);
        
        /*
        public ArrayList()
        Constructs an empty list with an initial capacity of ten.
        
        boolean add(E e)
        Appends the specified element to the end of this list (optional operation).
        
        boolean addAll(Collection<? extends E> c)
        Appends all of the elements in the specified collection to the end of this list,
        in the order that they are returned by the specified collection's iterator (optional operation). 
        */
        
        System.out.println(omenaLista);
        
        //teht 5
        /*Määrittele Abstrakti tehdas (Abstract Factory) Meijeri, josta saadaan kolmenlaisia tuotteita: maitoa, juustoa, jugurttia. 
        Toteuta kaksi erilaista konkreettista tehdasta: laktoositon ja vähälaktoosinen. 
        Kirjoita testiohjelma, jossa käytät joko laktoosittomia tai vähälaktoosisia tuotteita. 
        Tehdas voidaan vaihtaa muuttamatta soivelluskoodia muulla tavalla kuin tehtaan vaihdolla.
        
        Tuotteille pitää määritellä omat rajapinnat: interface Maito jne.
        */
      
        System.out.println("\nteht 5");
        Meijeri meijeri = new Laktoositon();
        
        Maito maito1 = meijeri.getMaito("maito");
        Juusto juusto1 = meijeri.getJuusto("juusto");
        Jugurtti jugurtti1 = meijeri.getJugurtti("jugurtti");
        
        System.out.println("\nLaktoosittomat tuotteet: ");
        maito1.tuota();
        juusto1.tuota();
        jugurtti1.tuota();
        
        meijeri = new Vahalaktoosinen();
        
        maito1 = meijeri.getMaito("maito");
        juusto1 = meijeri.getJuusto("juusto");
        jugurtti1 = meijeri.getJugurtti("jugurtti");
        
        System.out.println("\nVähälaktoosiset tuotteet: ");
        maito1.tuota();
        juusto1.tuota();
        jugurtti1.tuota();
        
        //teht 6
        System.out.println("\nteht 6");
        
        /*Määrittele vastuuketju (Chain of Responsibility), jossa käsitellään merkkijonona välitettävä dokumentti. 
        Ensimmäinen käsittelijä poistaa dokumentista kaikki ylimääräiset välilyönnit. 
        Toinen korvaa skandit seuraavasti: ä,å -> a; ö -> o; Ä,Å -> A; Ö -> O. 
        Kolmas tarkistaa oikeinkirjoituksen: kaikki sturct-sanat korvataan struct-sanoilla.*/
        
        UnaryOperator<Dokumentti> tuplavalilyonnit = (Dokumentti d) -> {
            if(d.getTeksti().contains("  ")){
                String text = d.getTeksti().replaceAll("(\\s+)", " ");
                d.setTeksti(text);
                System.out.println("Ylimääräiset välilyönnit poistettu dokumentista.");
                d.setKasitelty(true);
            }
            return d;
        };
        
        UnaryOperator<Dokumentti> korvaaSkandit = (Dokumentti d) -> {
            if(d.getTeksti().matches(".*[äåöÄÅÖ].*")) {
                String text = d.getTeksti();
                text = Normalizer.normalize(text, Normalizer.Form.NFD);
                text = DIACRITICS_AND_FRIENDS.matcher(text).replaceAll("");
                d.setTeksti(text);
                System.out.println("Skandit ovat korvattu.");
                d.setKasitelty(true);
            }
            return d;
        };
        
        UnaryOperator<Dokumentti> oikeinkirjoitukset = (Dokumentti d) -> {
            if(d.getTeksti().contains("sturct")) {
                String text = d.getTeksti().replaceAll("sturct", "struct");
                d.setTeksti(text);
                System.out.println("Oikeinkirjoitukset tehty.");
                d.setKasitelty(true);
            }
            return d;
        };
        
        Function<Dokumentti, Dokumentti> ketju = tuplavalilyonnit.andThen(korvaaSkandit).andThen(oikeinkirjoitukset);
        Dokumentti doku = new Dokumentti("Älämölölööö täällä  katsotaaan muumeja  ja hullutellaan sturct lailla!  sturct");
        System.out.println("Alkuperäinen:\n" + doku.getTeksti() + "\n");
        
        doku = ketju.apply(doku);
        System.out.println(doku.getTeksti());
        
        //teht 7
        System.out.println("\nteht 7");
        
        StrategyData data = new StrategyData((String s) -> {
            if(s.contains("  ")) {
                s = s.replaceAll("(\\s+)", " ");
                System.out.println("Tuplavälilyönnit poistettu.");
            }
            return s;
        });
        data.setTeksti("Aamumunkki  sturct  määmöö  sturct  shakkilauta  wååwää  sturct");
        data.muutaTeksti();
        System.out.println(data.getTeksti());
        
        data.muutaTekstiStrategialla((String s) -> {
            if(s.matches(".*[äåöÄÅÖ].*")) {
                s = Normalizer.normalize(s, Normalizer.Form.NFD);
                s = DIACRITICS_AND_FRIENDS.matcher(s).replaceAll("");
                System.out.println("Skandit ovat korvattu.");
            }
            return s;
        });
        
        System.out.println(data.getTeksti());
        
        data.muutaTekstiStrategialla((String s) -> {
            if(s.contains("sturct")) {
                s = s.replaceAll("sturct", "struct");
                System.out.println("Oikeinkirjoitukset tehty.");
            }
            return s;
        });
        
        System.out.println(data.getTeksti());
        
        //teht 8
        /*Uutistoimisto tuottaa uutisia, joita se välittää eri medioille (Helsingin Sanomat, Ilta-Sanomat jne.) 
        Kirjoita ja testaa Observer-mallin mukainen ratkaisu, jossa Helsingin Sanomat välittää lukijoilleen (tulostaa ruudulle) kaikki uutiset,
        joista löytyy avainsana “politiikka” ja Ilta-Sanomat jokaisen, josta löytyy avainsana "julkkis".*/
        System.out.println("\nteht 8");
        Uutinen uutinen = new Uutinen();
        
        List<String> hesari = new ArrayList<>();
        List<String> iltaSanomat = new ArrayList<>();
        
         uutinen.addObserver((Observable o, Object arg) -> {
             if(arg.toString().contains("politiikka")) {
                 hesari.add((String)arg);
             }
        });
         
         uutinen.addObserver((Observable o, Object arg) -> {
             if(arg.toString().contains("julkkis")) {
                 iltaSanomat.add((String)arg);
             }
        });
         
         Thread t1 = new Thread(uutinen);
        t1.start();
           
        try {
            t1.join();
        } catch (InterruptedException ex) {
        }
        
        hesari.forEach (System.out::println);
        iltaSanomat.forEach (System.out::println);
        
        
    }
    
}
