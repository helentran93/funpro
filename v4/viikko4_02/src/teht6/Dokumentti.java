/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package teht6;

import teht7.StrategyIF;

/**
 *
 * @author Helen
 */
public class Dokumentti {
    private boolean kasitelty = false;
    private String teksti;
    private StrategyIF strategy;
    
    public Dokumentti(String teksti) {
        this.teksti = teksti;
    }
    
    public String getTeksti() {
        return teksti;
    }
    
    public void setTeksti(String teksti){
        this.teksti = teksti;
    }
    
    public boolean isKasitelty() {
        return kasitelty;
    }
    
    public void setKasitelty(boolean kasitelty) {
        this.kasitelty = kasitelty;
    }
}
