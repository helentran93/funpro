/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package teht7;

/**
 *
 * @author Helen
 */
public class StrategyData {
    private String teksti;
    private StrategyIF strategy;
    
    public StrategyData(StrategyIF strategy) {
        this.strategy = strategy;
    }
    
    public String getTeksti() {
        return teksti;
    }
    
    public void setTeksti(String teksti){
        this.teksti = teksti;
    }
    
    public void muutaTeksti(){
        teksti = strategy.apply(teksti);
    }
    
    public void muutaTekstiStrategialla(StrategyIF strategy){
        teksti = strategy.apply(teksti);
    }
    
}
