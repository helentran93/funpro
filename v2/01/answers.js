// teht 1
let toCelsius = fahrenheit => (5/9) * (fahrenheit-32);

let area = radius => Math.PI * radius * radius;
console.log('teht 1');
console.log(toCelsius(100));
console.log(area(2));

// teht 2
var json = require('./movies.js');
console.log('teht 2');
console.log(json.map(x => ({title: x.title, release: x.release})));

// teht 3
console.log('teht 3');
console.log(json.filter(movie => movie.release >= 2011));

// teht 4
let helsinki15 = [-2, 1, 3, 6, 9, 13, 17, 18, 14, 7, -6, 3];
let helsinki16 = [-8, -1, 2, 5, 14, 16, 17, 16, 13, 6, -2, -3];

var keskilampotila = function(taulu1, taulu2) {
    var uusi = [];
    for(var i = 0; i < taulu1.length; i++) {
        uusi.push((taulu1[i] + taulu2[i]) / 2);
    }
    
    var positiiviset = uusi.filter(pos => pos > 0);
    return positiiviset.reduce((sum, value) => (sum + value)) / positiiviset.length;
};

console.log('teht 4');
console.log(keskilampotila(helsinki15, helsinki16));

// teht 5
var fs = require("fs");
var text = fs.readFileSync("./kalevala.txt", "utf-8").toLowerCase();
var sanat = text.match(/[^\.;,:!\s-][\wÅÖÄäöå']*/g);

var lista = {};

sanat.map(sana => {
    var total = sanat.reduce(function(total,x){return x==sana ? total+1 : total}, 0);
    lista[sana] = total;
});

console.log('teht 5');
console.log(lista);