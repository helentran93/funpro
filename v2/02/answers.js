// teht 1
function laskePisteet(kPiste, lisapisteet) {
    var hyppaaja = 0;
    return function(pituus) {
        // jos on normaalimäki
        if(lisapisteet == 2.0) {
            if(pituus < kPiste) {
                return hyppaaja -= (2.0 * (kPiste - pituus));
            } else if(pituus == kPiste) {
                return hyppaaja += 60;
            } else {
                return hyppaaja += (60 + 2.0 * (pituus - kPiste));
            }
        }
        // jos on suurmäki
        else {
            if(pituus < kPiste) {
                return hyppaaja -= (1.8 * (kPiste - pituus));
            } else if(pituus == kPiste) {
                return hyppaaja += 60;
            } else {
                return hyppaaja += (60 + 1.8 * (pituus - kPiste));
            }
        }
    };
}

console.log('\nTeht 1');
const normaali = laskePisteet(95, 2.0);
console.log('\nNormaalimäki');
console.log(normaali(95));
console.log(normaali(88));

const suuri = laskePisteet(100, 1.8);
console.log('\nSuurmäki');
console.log(suuri(120));
console.log(suuri(99));

// teht 2
const Auto = (function() {
    const suojattu = new WeakMap();
    
    class Auto {
        constructor() {
            suojattu.set(this, {matkamittari: 0});
            this.tankki = 0;
        }
        
        getKilometrit() {
            return suojattu.get(this).matkamittari;
        }
        
        getTankki() {
            return this.tankki;
        }
        
        setTankki(maara) {
            this.tankki =+ maara;
        }
        
        aja() {
            if(this.tankki > 0) {
                suojattu.get(this).matkamittari++;
                this.tankki--;
            } else {
                console.log("Ei ole bensaa, tankkaa!");
            }
        }
    }
    
    return Auto;
})();

const auto = new Auto();
console.log("\nteht 2");
console.log("Polttoaineen määrä: " + auto.getTankki());
auto.setTankki(7);
console.log("Polttoaineen määrä: " + auto.getTankki());
console.log("Ajettu matka: " + auto.getKilometrit() + " km");

for(var i = 0; i < 10; i++){
    auto.aja();
    console.log("Polttoaineen määrä: " + auto.getTankki());
    console.log("Ajettu matka: " + auto.getKilometrit() + " km");
}

console.log("\nSuojattu matkamittari: " + auto.matkamittari); // ei pidä näkyä; undefined
console.log("Ei suojattu tankki: " + auto.tankki);   // näyttää tankin tilan

// teht 3
const Immutable = require('immutable');
const set1 = Immutable.Set(['punainen', 'vihreä', 'keltainen']);

console.log("\nteht 3");
const set2 = set1 & 'ruskea';
console.log(set1 === set2);
//set 1-taulukossa ei ole 'ruskea' ja siksi set2 ja set1 taulukkoina eivät ole samoja
//sillä set1-taulukko on muuttumaton. set2-taulukkoon lisätään set1-alkiot sekä 'ruskea'

const set3 = set2 & 'ruskea';
console.log(set2 === set3);
//set2 ja set3 ovat samoja, koska molemmat ovat muutettavia taulukoita ja sisällöltään
//samoja. set2-taulukossa ei esiinny 'ruskea' toista kertaa, koska Set ei hyväksy
//duplikaatteja

/*

*/

// teht 4
// laiska
console.log('\nteht 4');
let varit = Immutable.Seq(["punainen", "keltainen", "vihreä", "keltainen", "pinkki"])
            .map(function(x){console.log(`map ${x}`); return x});
console.log(`${varit.get(1)}`);
            
// innokas
let varit2 = ["punainen", "keltainen", "vihreä", "keltainen", "pinkki"]
            .map(function(x){console.log(`map ${x}`); return x});
console.log(`${varit2[1]}`);

// teht 5
var firstTenElements = Immutable.Repeat()
                                .map( Math.random )
                                .take( 10 )
                                .toJSON();

console.log('\nteht 5');
console.log(firstTenElements);

var toista = Immutable.Repeat("TOISTA");
console.log("\n" + toista);

var toista2 = Immutable.Repeat("TOISTA", 8)
                       .toJSON();
console.log(toista2);

/*
Immutable.Repeat() palauttaa arvon toistettuna äärettömästi ellei
määritetä lukua, jonka mukaan se toistaisi. Esimerkissä aina uudella
kierroksella palauttaa uuden luvun (map-funktio) ja tulostaa sen, kunnes
ollaan saatu 10 ensimmäistä lukua.
*/