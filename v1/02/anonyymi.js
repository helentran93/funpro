'use strict'

const a = function () {
    return function (x) {
		return x+1;
	 }
	}(); // Huomaa funktion kutsu!

let tulos = a(3);

console.log(tulos);

// teht. 1
const verrataan = function () {
	return function(a, b) {
		if(a > b) {
			return 1;
		} else if(a < b) {
			return -1;
		} else {
			return 0;
		}
	}
}();

let testi = verrataan(1, 0);
console.log(testi); // 1
testi = verrataan(1, 2);
console.log(testi); // -1
testi = verrataan(1, 1);
console.log(testi); // 0

// teht. 2
let helsinki15 = [-2, 1, 3, 6, 9, 13, 17, 18, 14, 7, 6, 3];
let helsinki16 = [-8, 1, 2, 5, 14, 16, 17, 16, 13, 6, 3, -2];

function annaLampotilat(funktio, taulukko1, taulukko2) {
	let counter = 0;
	for(let i = 0; i < taulukko1.length; i++) {
		if(funktio(taulukko1[i], taulukko2[i]) === 1) {
			counter++;
		}
	}
	return counter;
}

console.log(annaLampotilat(verrataan, helsinki16, helsinki15));

// teht. 3
const offset = [1,2];
const zoom = 2;
const phi = Math.PI;

const point = { x: 1, y: 1};

const pipeline  = [   // 2D-muunnoksia
    
    function translate(p){
        return { x: p.x + offset[0], y: p.y + offset[1] };
    },

    function scale(p){
        return { x: p.x * zoom, y: p.y * zoom};
    },
    
    function rotate(p){
        return { x: p.x * Math.cos(phi) - p.y * Math.sin(phi), y: p.x * Math.sin(phi) + p.y * Math.cos(phi)};
    }
];


function muunnos(point){
     for(let i=0; i<pipeline.length; i++){   
        point = pipeline[i](point);
    }
    return point;
}


console.log(point);
console.log(muunnos(point));

// teht. 4
function potenssiin(l, p) {
  if (p === 0) {
    return 1;
  }
  return l * potenssiin(l, --p);
}

var pow = potenssiin(2, 4);
console.log(pow);
pow = potenssiin(2, 0);
console.log(pow);