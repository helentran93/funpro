'use strict';

let f, g;
function foo() {
  let x;
  f = function() { return ++x; };
  g = function() { return --x; };
  x = 1;
  console.log('inside foo, call to f(): ' + f());
}
foo();  
console.log('call to g(): ' + g()); 
console.log('call to f(): ' + f()); 

// teht. 5
/*
Esimerkissä foo()-funktion sisällä alustetaan määritellään paikallinen muuttuja x ja funktion ulkopuolella vapaat muuttujat f ja g.
Vapaat muuttujat suljetaan funktion suoritukseen mukaan eli muodostuu "sulkeuma".
Kutsutaan foo()-funktiota kerran ja sen tuloksena tulostetaan konsoliin f()-funktion palauttaman x-muuttujan arvon eli 2.
Lopussa paikallinen muuttuja x:n arvo alustetaan 1.
Sitten kutsutaan f() ja g()-funktiot. g()-funktio palauttaa arvon 1 ja f()-funktio taas arvon 2.

++x tarkoittaa x-muuttujan arvon kasvattamista yhdellä ja palauttaa uuden arvon samantien. Sama juttu --x kanssa, mutta päinvastoin.
Paikallisesti luotu muuttuja x tuhoutuu foo()-funktion kutsun jälkeen, mutta muuttuja x:n arvo on sisällä olevien funktioiden käytettävissä
paluuarvona.
*/