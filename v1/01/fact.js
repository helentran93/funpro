function fact(n) {
// triviaalitapaus
  if (n === 0) {
    return 1;
  }
 // perussilmukka
  return n * fact(n - 1);
}
var tulos = fact(4);
console.log(tulos);

//tehtävä 1
function onPalindromi(merkkijono) {
  
  if(merkkijono.length === 0 || merkkijono.length === 1) {
    return true;
  } else if (merkkijono.charAt(0) !== merkkijono.slice(-1)) {
    return false;
  } else {
    var m = merkkijono.slice(1, -1);
    return onPalindromi(m);
  }
}

var imaami = onPalindromi("imaami");
var mommy = onPalindromi("mommy");
var sana = onPalindromi("alliana");

console.log(imaami);
console.log(mommy);
console.log(sana);

//tehtävä 2
function syt(p, q) {
  if (q === 0) {
    return p;
  } else if(p % q === 1) {
    return "luvut eivät ole jaollisia!";
  } else {
    return syt(q, p % q);
  }
}

var luku1 = syt(102, 68);
var luku2 = syt(35, 18);

console.log(luku1);
console.log(luku2);

// tehtävä 3

function kjl(p, q) {
  if(p % q === 1) {
    return 1;
  } else if(p % q === 0) {
    return "luvut eivät ole jaottomia!";
  } else {
    return kjl(q, p % q);
  }
}

var jaoton1 = kjl(35, 18);
var jaoton2 = kjl(102, 68);

console.log(jaoton1);
console.log(jaoton2);

// tehtävä 4
function potenssiin(l, p) {
  if (p === 0) {
    return 1;
  } else {
    return l * potenssiin(l, --p);
  }
}

var pow = potenssiin(2, 4);
console.log(pow);
pow = potenssiin(2, 0);
console.log(pow);

// tehtävä 5

function kaannaLista(lista) {
  var kaannettava = [];
  function kaantaja(lista) {
    if(lista.length !== 0) {
      kaannettava.push(lista.pop());
      kaantaja(lista);
    }
  }
  kaantaja(lista);
  return kaannettava;
}

var lista = [0, 1, 2, 3, 4];
console.log(kaannaLista(lista));