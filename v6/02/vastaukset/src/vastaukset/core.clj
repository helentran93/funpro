(ns vastaukset.core
  (:gen-class))

(defn -main
  "I don't do a whole lot ... yet."
  [& args]
)

;teht 1
(println "teht. 1")
;korjaus tehtävään 1
(defn allekirjoitus1
  [tervehdys nimi]
  (println tervehdys nimi)
)

(def fin (partial allekirjoitus1 "Ystävällisin terveisin,"))
(fin "helen")
;-------------------------

(def allekirjoitus
  (partial "Ystävällisin terveisin,")
)

(def signature
  (partial "Best regards,")  
)

(def svenska
  (partial "Med vänliga hälsningar,")
)

(println allekirjoitus "Helen")
(println signature "Helen")
(println svenska "Helen")

;teht 2
(println "\nteht. 2")

(def vektori [[1 2 3][4 5 6][7 8 9]])

;a
(def minimit (map #(apply min %) vektori))

(println minimit)

;b
(def muutettu (apply vector minimit))

(println muutettu)

;teht 3
(println "\nteht. 3")

(def file "sima.csv") 
 
(def aineet [:aines :yksikko :maara])

(defn str->int
  [str]
  (Integer. str))

(def konversiot {:aines identity
                 :yksikko identity
                 :maara str->int})

(defn konvertoi 
  [avain arvo]
  ((get konversiot avain) arvo))



(defn parse 
   "parse tuottaa merkkijonovektoreita:  ['Vesi' 'litraa' '10'] jne."
  [string]
  (map #(clojure.string/split % #",")
       (clojure.string/split string #"\n")))

     
(defn tee-mappeja 
  "tuottaa merkkijonovektoreista mappeja seq-peräkkäisrakenteeksi"
  [merkkijonovektoreista]
  (map (fn [merkkijonovektori]  ; mappausfunktio on mapin 1. arg
            (reduce (fn [uusi-map [avain arvo]]  ; uusi-map on alussa tyhjä map
               (assoc uusi-map avain (konvertoi avain arvo)) ; (assoc map key val) 
            )
            {}  ; reducen 2. arg alussa tyhjä map
            (map vector aineet merkkijonovektori)) ; reducen 3. arg tuottaa seq:n avain-arvo -vektoreita
        )
        merkkijonovektoreista) ; mapin 2. arg (luettu tiedostosta)
)
  

             
(tee-mappeja (parse (slurp file)))

(def omasima (tee-mappeja (parse (slurp file))))
           
(def pullosima (apply vector omasima)) 
(vector? pullosima) ;=> true
(println omasima)

(defn kerrotaan?
  [monikertoja]
  (map #(update % :maara * monikertoja) pullosima)
)

(println "Kaksinkertainen:" (kerrotaan? 2))
(println "Kolminkertainen:" (kerrotaan? 3))
(println "Kymmenkertainen:" (kerrotaan? 10))