(ns vastaukset.core
  (:gen-class))

(defn -main
  "I don't do a whole lot ... yet."
  [& args])

;teht 1
(println "teht. 1")
(def lista1 [-1, 0, 6, 8, 12, 18, 20, 15, 10, 9, 5, -3])
(def lista2 [-4, -1, 4, 6, 13, 18, 22, 12, 9, 5, -2, -6])

(def keskiarvot
  (map #(/ % 2)
    (map + lista1 lista2)
  )
)

(def positiiviset 
  (filter #(> % 0) keskiarvot)
)

(def keskilampotila
    (format "%.1f" (double (/ (reduce + positiiviset) (count positiiviset))))
)

(println "Keskilämpötila: " keskilampotila)

;teht 2
(println "\nteht. 2")
(def food-journal
  [{:kk 3 :paiva 1 :neste 5.3 :vesi 2.0}
   {:kk 3 :paiva 2 :neste 5.1 :vesi 3.0}
   {:kk 3 :paiva 13 :neste 4.9 :vesi 2.0}
   {:kk 4 :paiva 5 :neste 5.0 :vesi 2.0}
   {:kk 4 :paiva 10 :neste 4.2 :vesi 2.5}
   {:kk 4 :paiva 15 :neste 4.0 :vesi 2.8}
   {:kk 4 :paiva 29 :neste 3.7 :vesi 2.0}
   {:kk 4 :paiva 30 :neste 3.7 :vesi 1.0}]
)
 
(def huhtikuut
  (filter #(> (% :kk) 3) food-journal)
)

(def nestekulutus
    (format "%.1f" (double (- (reduce #(+ %1 (%2 :neste)) 0 huhtikuut) (reduce #(+ %1 (%2 :vesi)) 0 huhtikuut))))
)

(println huhtikuut)
(println "Nestekulutukset ilman vettä: " nestekulutus)

;teht. 3
(println "\nteht. 3")

(def yhteen
    (map #(assoc % :muuneste (format "%.1f" (- (% :neste) (% :vesi)))) huhtikuut)    
)

(def uusi
    (map #(apply dissoc % [:neste :vesi]) yhteen)  
)

(println uusi)
