/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package main;

import java.util.Arrays;
import java.util.function.DoubleUnaryOperator;
import java.util.function.Supplier;
import java.util.stream.IntStream;

/**
 *
 * @author helentr
 */
public class Main {

    /**
     */
    
    // teht 1
    public static class Tulostaja{
        public void tulosta(Supplier source){
            System.out.println(source.get());
        }
    }
    
    // teht 2
    public static DoubleUnaryOperator makePistelaskuri(double kPiste, double lisapisteet){
            return (pituus) ->  {
                int hyppaaja = 0;
                if(pituus < kPiste) {
                    return hyppaaja -= (lisapisteet * (kPiste - pituus));
                } else if (pituus == kPiste) {
                    return hyppaaja += 60;
                } else {
                    return hyppaaja += (60 + lisapisteet * (pituus - kPiste));
                }
            };// lambda tähän
    }
    
    public static void main(String[] args) {
        // TODO code application logic here
        // GeneratorFIF:n toteutuksia:
    
        // teht 1
        System.out.println("\nUusi esimerkki Supplier-rajapinnalla: ");
        
        Supplier generatorNewWay = (Supplier) () -> 3;
        
        Supplier gen1 = (Supplier) () -> 2;
        Supplier gen2 = (Supplier) () -> (int) (Math.random() * 6 + 1);
        
        Tulostaja t = new Tulostaja();
        
        t.tulosta(generatorNewWay);
        t.tulosta(gen1);
        t.tulosta(gen2);
        t.tulosta(() -> 100);
        
        // teht 2
        DoubleUnaryOperator normaaliLahti = makePistelaskuri(90, 2.0);
       
        System.out.println("\nteht. 2");
        System.out.println(normaaliLahti.applyAsDouble(100));
        
        // teht 3
        System.out.println("\nteht. 3");
        
        // 1. tapa
        int[] array = IntStream.generate(() -> 1 + (int) (Math.random() * ((22 - 1) + 1)))
                .limit(10)
                .toArray();
        
        System.out.println(Arrays.toString(array));
        
        // 2. tapa
        // https://stackoverflow.com/questions/22637900/java8-lambdas-vs-anonymous-classes
    }
    
}
