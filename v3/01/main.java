/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Main;

import java.io.IOException;
import java.nio.charset.Charset;
import java.util.Arrays;
import java.util.List;
import java.util.Map;
import java.util.Random;
import java.util.function.DoubleFunction;
import java.util.stream.Collectors;
import static java.util.stream.Collectors.toList;
import teht2.Dish;
import teht2.Dish.Type;
import static teht2.Dish.menu;
import teht2.Trader;
import teht2.Transaction;
import java.nio.file.*;
import java.util.function.Function;

/**
 *
 * @author helentr
 */

public class Main {
    public static void main(String[] args) throws IOException {
    
        // teht 1
        DoubleFunction toCelsius = fahrenheit -> (5.0 / 9) * (fahrenheit-32);
        DoubleFunction area = radius -> Math.PI * radius * radius;
        
        System.out.println("teht 1");
        System.out.println("10 Fahrenheit to Celsius: " + toCelsius.apply(10));
        System.out.println("Calculating area with 2m: " + area.apply(2));
        
        // teht 2
        Trader raoul = new Trader("Raoul", "Cambridge");
        Trader mario = new Trader("Mario","Milan");
        Trader alan = new Trader("Alan","Cambridge");
        Trader brian = new Trader("Brian","Cambridge");
		
        List<Transaction> transactions = Arrays.asList(
            new Transaction(brian, 2011, 300), 
            new Transaction(raoul, 2012, 1000),
            new Transaction(raoul, 2011, 400),
            new Transaction(mario, 2012, 710),	
            new Transaction(mario, 2012, 700),
            new Transaction(alan, 2012, 950)
        );	
        
        // Etsi kaikki rahansiirrot (transaction), jotka ovat tehty vuoden 2012 jälkeen ja joiden arvo on vähintään 900 
        List<Transaction> tr2011 = transactions.stream()
                                               .filter(transaction -> transaction.getYear() > 2011)
                                               .filter(transaction -> transaction.getValue() >= 900)
                                               .collect(toList());
        
        // Laske eri ruokalajien määrä (Dish.java-tiedostossa on lueteltu ruokalajeja) käyttäen map- ja reduce-operaatioita.
        Map<Type, Long> tyypit = menu.stream()
                .collect(Collectors.groupingBy(Dish::getType, Collectors.counting()));
        
        System.out.println("\nteht 2");
        System.out.println(tyypit);
        System.out.println(tr2011);
        
        // teht 3
        
        // Tee vuo, jossa generoit 20 satunnaislukua, muutat ne nopan silmäluvuiksi sopiviksi välille 1-6, ja lasket kuinka monta kutosta saadaan.
        System.out.println("\nteht 3");
        
        // https://howtodoinjava.com/java-8/stream-random-numbers-range/
        Random rnd = new Random();
        List<Integer> nums = rnd.ints(20, 1, 7)
                .boxed()
                .collect(Collectors.toList());
        
        int counts = (int) nums.stream()
                .filter(n -> n == 6)
                .count();
        
        System.out.println(nums);
        System.out.println("Kutosten määrä: " + counts);
        
        // teht 4
        System.out.println("\nteht 4");
        
        // https://stackoverflow.com/questions/25048010/should-i-use-java-8-streams-api-to-combine-two-collections
        List<Integer> taulu1 = Arrays.asList(1, 2, 3);
        
        List<Integer> taulu2 = Arrays.asList(3, 4);
        
        List<int[]> parit = taulu1.stream()
                .flatMap(a -> taulu2.stream()
                .map(b -> new int[]{a, b}))
                .collect(Collectors.toList());
        
        // https://stackoverflow.com/questions/3442090/java-what-is-this-ljava-lang-object
        System.out.println("1. lista: " + taulu1 +
                "\n2. lista: " + taulu2 +
                "\nUusi lista: " + Arrays.deepToString(parit.toArray()));
        
        // teht 5
        System.out.println("\nteht 5");
        
        /*
            Hakemistossa on kalevala.txt-niminen tiedosto, jossa on Kalevalan alusta pieni pätkä. 
            Käytä hyväksesi Javan NIO APIa ja siellä määriteltyä java.nio.file.Files.lines-metodia, joka palauttaa streamin. 
            Katso esimerkkiä tiedostosta stream-hakemistosta BuildingStreams.java-tiedostosta. 
            Esimerkissä etsitään, kuinka monta eri sanaa esiintyy tiedostossa. 
            Muokkaa esimerkkiä ja tee samanlainen esiintymälista kuin Javasriptillä viime viikolla.
        */
        
        Map<String, Long> sanat = Files.lines(Paths.get("src/teht5/kalevala.txt"), Charset.defaultCharset())
                .flatMap(line -> Arrays.stream(line.split(" ")))
                .map(sana -> sana.replaceAll("[.;,:!\n\\s]", "").toLowerCase())
                .collect(Collectors.groupingBy(Function.identity(), Collectors.counting()));
                
        System.out.println(sanat);
    }
}
