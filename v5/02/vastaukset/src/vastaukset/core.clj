(ns vastaukset.core
  (:gen-class))

(defn -main
  "I don't do a whole lot ... yet."
  [& args]
  (println "Hello, World!"))

;teht 1
(println "teht 1")
(println "Anna luku")
(def parillinen?
  (let [luku(read-line)]
      (if (> (Integer. luku) 0) 
        (if (= (mod (Integer. luku) 2) 0)
          (println "Luku on parillinen")
          (println "Luku ei ole parillinen")
        )
      (println "Annoit liian pienen luvun.")
      )
  )
)

;teht 2
(println "\nteht 2")
(println "Anna luku")
(def parillinen?
  (let [luku(read-line)] ;sidotaan uusi arvo muuttujalle 'luku'
       (loop [luku luku]
            (if (> (Integer. luku) 0) 
              (if (= (mod (Integer. luku) 2) 0)
                (println "Luku on parillinen")
                (println "Luku ei ole parillinen")
              )
              (recur (read-line)) ;pyydetään uusi arvo käyttäjältä
            )
        )
  )
)

;teht 3
(println "\nteht 3")
(println "Anna luku")
(def jaollinen?
  (let [luku(read-line)]
    (println "Kolmella jaolliset: ")
     (loop [luku luku]
      (when (> (Integer. luku) 1)
        (if (= (mod (Integer. luku) 3) 0)
          (println luku)
        )
        (recur (dec (Integer. luku)))
      )
    )
  )
)

;teht 4
(println "\nteht 4")
(defn lotto
  []
  (loop [s #{}]
    (if (< (count s) 7)
      (recur (into s [(+ (rand-int 38) 1)]))
      s ;palauttaa joukon
    )
  )  
)

(println (lotto))

;teht 5
(println "\nteht 5")
(defn syt
  [luku1 luku2]
  (if (= (Integer. luku2) 0)
    (println "Suurin luku on : " luku1)
    (recur luku2 (mod luku1 luku2))
  )
)

(println (syt 102 0))
(println (syt 102 68))