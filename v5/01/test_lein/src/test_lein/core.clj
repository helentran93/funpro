(ns test-lein.core
  (:gen-class))

(defn -main
  "I don't do a whole lot ... yet."
  [& args]
  (println "Hello, World!")
)

;teht. 2
(println "teht. 2")
(+ (* 2 5) 4)
( + 1 2 3 4 5)
(println ((fn [name] (str "Tervetuloa Tylypahkaan " name)), "Helen"))
(println (get-in {:name {:first "Urho" :middle "Kaleva" :last "Kekkonen"}} [:name :middle]))

;teht. 4
(println "\nteht. 4")
(defn square [x] (* x x))
(println(square 2))
(println(square 7))

;teht. 5
(println "\nteht. 5")
(defn karkausvuosi? [x]
  (cond (zero? (mod x 400)) true
        (zero? (mod x 100)) false
        (zero? (mod x 4)) true
        :default false
  )
)
(println(karkausvuosi? 100))
(println(karkausvuosi? 200))
(println(karkausvuosi? 400))
(println(karkausvuosi? 12))
(println(karkausvuosi? 20))
(println(karkausvuosi? 15))
